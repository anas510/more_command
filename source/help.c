#include <stdio.h>

//extern unsigned short LINELEN;

void help(){
	// FILE* fp = fopen("help.txt","r");

	// char buffer[LINELEN];
	// while (fgets(buffer, LINELEN, fp)!=NULL){
	//    fputs(buffer, stdout);
	// }
	// printf("\n");
	//fclose(fp);

	printf("------------------------------------------------------------------------------------------\n");
	printf("<space>         	Display next k lines of text [k=current screen size]\n");
	printf("z               	Display next k lines of text [k=current screen size]\n");
	printf("<return>        	Display next line of text\n");
	printf("q or Q   		Exit from more (quits current file in case of multiple files)\n");
	printf("=               	Display current line number\n");
	printf("h or ?			To get help\n");
	printf("v               	Start up /usr/bin/vi at current line\n");
	printf(":n              	Go to next file\n");
	printf(":p              	Go to previous file (For 1st file it will display that file again)\n");
	printf(":f                  	Display current file name and line number\n");
	printf("/<search_string>    	Search for string in file (first occurence), after current position\n");
	printf("------------------------------------------------------------------------------------------\n");
}