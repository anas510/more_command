#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "main.h"

extern long total_lines;
extern long lines_displayed;
extern unsigned short PAGELEN;
extern unsigned short LINELEN;

//Used in case if user has entered multiple files
//As simple do_more function doesn't print "Next File" prompt so it is handled here with alot of checks

//It returns integer to increment/decrement file number to move to next/previous file respectively.
//0 = next file
//-1 = no previous file available so start file again
//-2 = move to previous file
// 2 = no next file available so break loop and end program
int process_multiFile_input(char* file,char* prev,char* next, int rv,int argc){
   if (rv == 0){           //user pressed q or Q
      printf("\033[2K\033[1G");   
      exit(1);
   }
   else if (rv == 3){         //user pressed '='
      printf("\033[2K \033[1GLine-%ld",lines_displayed);
   }
   else if (rv == 4){         //user pressed h
      printf("\n");
      help();
      printf("\033[7m --More-- (Next file: %s) \033[m", file);
   }
   else if (rv == 5){         //user pressed v
      if(isatty(STDIN_FILENO)){     
         //In case stdin_redirection infinite loop will start because vim will try to get 
         //input from stdin but it is redirected so it will have to use /dev/tty
         launch_vim(file);
         printf("\033[2K \033[1Gvim %s-------------\n",file);
         printf("\033[7m --More-- (Next file: %s) \033[m", file);  
      }
      else{
         printf("\033[2K \033[1GVim can't be used with input_redirection\n");  
      }
   }
   else if (rv == 6){         //user typed :f   (fileName + Line)
      printf("\033[2K \033[1G\"%s\" Line-%ld",file,lines_displayed);
   }
   else if (rv == 7){         //user typed :n   (next File)
      printf("\033[2K \n\033[1G...Skipping\n");  
      if(next!=NULL){
         printf("...Skipping to next file %s\n\n",next);
         return 0;
      }       
      return 2;
   }
   else if (rv == 8){         //user typed :p   (prev File)
      printf("\033[2K \n \033[1G...Skipping\n");            
      if(prev!=NULL){
         printf("...Skipping back to file %s\n\n",prev);
         return -2;
      }
      else if(argc>1){       //Here file can never be "stdin"
         printf("...Skipping back to file %s\n\n",file);
         return -1;
      }
   }
   else if (rv == 9){         //user typed /   (search String Forward)
      //input string then search it
      FILE* fp_tty = fopen("/dev/tty", "r");
      char* str=dynamicString(fp_tty);
      fclose(fp_tty);
      printf("\033[2K\033[1G");
      printf("\033[7mPattern Not Found\033[m"); 
      free(str);
   }
   else if (rv == -1){         //invalid character
      printf("\033[2K\033[1G");      
   }

   return -3;
}