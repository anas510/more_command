#include <stdio.h>
#include <main.h>
/* 
   To get input from passed stream FILE*
*/

int get_input(FILE * cmdstream)
{
   set_input_mode();
   int c;
   
   while((c=getc(cmdstream)) != EOF){
      if(c == 'q' || c == 'Q')
         return 0;
      else if ( c == ' ' || c == 'z')               
         return 1;   
      else if ( c == '\n' )  
         return 2;      
      else if ( c == '=' )               
         return 3;   
      else if ( c == 'h' || c == '?')  
         return 4;      
      else if ( c == 'v' )               
         return 5;   
      else if ( c == ':' ){
         c=getc(cmdstream);
         
         if(c=='f')
            return 6;  
         else if(c=='n')
            return 7;   
         else if(c=='p')
            return 8;   
      }
      else if(c=='/'){
         printf("\033[2K\033[1G/");
         return 9;
      }
   }
   
   return -1;        //do nothing
}