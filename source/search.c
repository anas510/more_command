#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern unsigned short LINELEN;

char* dynamicString(FILE* fp_tty){
   char* line = (char*)malloc(1);
   line[0] = '\0';   //this ensures that if user press enter without enterying anything the program do not crash
   char ch;
   int bytes_allocated = 0;
   do{
      //Store user character
      ch = getc(fp_tty);

      //whenever user press Enter key, break the loop
      if(ch == '\n')
         break;
      else{
         putchar(ch);
      }
      //Is this the first character getchar have read from I/p Q (buffer)
      if(line[0] == '\0')
         line = (char*)realloc(line,2);   //allocate two bytes
      else //otherwise
         line = (char*)realloc(line, bytes_allocated +2);
      //the user enters a sentance, the program gets the input character by character
      // and the size keeps on growing dynamically as the user keeps entrying
      line[bytes_allocated++] = ch;
   }while(ch!= '\n');

   line[bytes_allocated] = '\0';
   return line;
}

int searchStr(char* find,FILE* fp){
   char c1[LINELEN];
   long ptr=ftell(fp);
   while(fgets(c1, LINELEN, fp) != NULL) {
      if((strstr(c1, find)) != NULL) {    /* string is found   (returns NULL if not found)*/
         fseek(fp, -strlen(c1)*3, SEEK_CUR);    //to move back 3 lines (as we also want to print 2 lines above)
         return 1;
      }
   }

   //To move pointer back to original position if string not found
   fseek(fp, ptr, SEEK_SET);
   return 0;
}