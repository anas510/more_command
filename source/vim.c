#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void launch_vim(char* file){
   //Code to fork new process for VIM
   int cpid = -1,status;
   cpid = fork();
   
   if (cpid == -1)
   {
      perror("Fork Failed");
      exit (1);
   }
   if (cpid == 0) //child code
   {
      execlp("/usr/bin/vi","myvim",file,NULL);
      perror("exec Failed");
      exit(1);
   }
   else       //parent code
   {
      wait(&status);
   }
}