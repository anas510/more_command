#include <unistd.h>
#include <stdio.h>			//For FILE*
#include <sys/ioctl.h>

extern unsigned short LINELEN;

//To calculate total_lines in file for file percentage
long total_size(FILE* fp){
      long lines = 0;
      char buff[LINELEN];
      while(fgets(buff,LINELEN, fp))
            lines++;
      fseek(fp, 0, SEEK_SET);
      return lines;
}

//To get current terminal rows
int shell_lines()
{
	FILE* fp=fopen("/dev/tty","r+");
	//To get attributes of file with descriptor fileNo which is "/dev/tty"
	int fileNo=fileno(fp);
	
	struct winsize w;
	ioctl(fileNo, TIOCGWINSZ, &w);
	fclose(fp);
	return w.ws_row;
}