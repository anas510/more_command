#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "main.h"

extern long total_lines;
extern long lines_displayed;
extern unsigned short PAGELEN;
extern unsigned short LINELEN;

//Perform actions based on user input and control MORE program

//It returns integer to increment/decrement file number to move to next/previous file respectively.
//0 = next file
//-1 = no previous file available so start file again
//-2 = move to previous file
//-4 means do_more returned normally (complete file is read without skipping anything till EOF)
int do_more(FILE *fp,char* file,char* prev,char* next, int rv,int argc)
{
   short percStr=1;           //If 0 then percentage string will not be displayed
   short search=0 ;           
   //To erase message prompt (if user presses h after searching) 
   //as we don't remove percString in case if user presses h
   char buffer[LINELEN];
   int num_of_lines;        //PAGELEN
   FILE* fp_tty = fopen("/dev/tty", "r");
   
   if (fp_tty == NULL)
      exit(1);

   if(rv==2){
      num_of_lines=PAGELEN-1;
   }
   else if(rv!=10)
      num_of_lines=0;
   else if(rv==10)
      num_of_lines=2;

   while (fgets(buffer, LINELEN, fp)){
      if(rv!=-1 || rv == 1 || rv == 2 || rv==10){
         fputs(buffer, stdout);
         ++num_of_lines;
         ++lines_displayed;    //for calculating percentage
      }
      else{       //To moveback 1-line in case of h,void(returns -1)
         fseek(fp, -strlen(buffer), SEEK_CUR);
      }

      if (num_of_lines == PAGELEN){

         if((rv==-1 || rv==-2 || rv==1 || rv==2 || rv==-3 || rv==10)&&(search==0)){            
            if(strcmp(file,"stdin")!=0 && percStr==1){    //In case of input redirection FPE will occur becasue total_lines==0
               int percentage = 100*lines_displayed/total_lines;
               printf("\033[7m --More-- (%d%%) \033[m", percentage);            //to show % of file 
            }
            else if (percStr==1){
               printf("\033[7m --More-- \033[m");
            }

            rv = get_input(fp_tty);
         }
         else{
            rv = get_input(fp_tty);
            search=0;
         }
         
         if (rv == 0){              //user pressed q or Q
            /*
               1A => To move 1 line up    (no need to use in non-canonical mode)
               2K => To clear that line
               1G => To move cursor to Column-1
            */
            printf("\033[2K\033[1G");   
            exit(1);
         }
         else if (rv == 1){         //user pressed space bar
            num_of_lines -= PAGELEN;      //show next screen
            printf("\033[2K\033[1G");  
         }
         else if (rv == 2){         //user pressed return/enter
            printf("\033[2K \033[1G");
            num_of_lines -= 1; //show one more line            
         }
         else if (rv == 3){         //user pressed '='
            printf("\033[2K \033[1GLine-%ld",lines_displayed);            
            percStr=0;
            rv=-1;
            continue;    
         }
         else if (rv == 4){         //user pressed h
            if(search==1)
               printf("\033[2K \033[1G");
            printf("\n");
            help();
            rv=-1;
            continue;                       
         }
         else if (rv == 5){         //user pressed v
            if(isatty(STDIN_FILENO)){     
               //In case stdin_redirection infinite loop will start because vim will try to get 
               //input from stdin but it is redirected so it will have to use /dev/tty
               launch_vim(file);
               printf("\033[2K \033[1Gvim %s-------------\n",file);
            }
            else{
               printf("\033[2K \033[1GVim can't be used with input_redirection\n");
            }
            rv=-1;
            continue;
         }
         else if (rv == 6){         //user typed :f   (fileName + Line)
            printf("\033[2K \033[1G\"%s\" Line-%ld",file,lines_displayed);            
            percStr=0;
            rv=-1;
            continue;    
         }
         else if (rv == 7){         //user typed :n   (next File)
            printf("\033[2K \n\033[1G...Skipping\n");  
            if(next!=NULL || (argc>1 && strcmp(file,"stdin")!=0) ){
               printf("...Skipping to next file %s\n\n",next);
               return 0;
            }        
            break;       //To break loop if no more file
         }
         else if (rv == 8){         //user typed :p   (prev File)
            printf("\033[2K \n \033[1G...Skipping\n");            
            if(prev!=NULL){
               printf("...Skipping back to file %s\n\n",prev);
               return -2;
            }
            else if( argc>1 && strcmp(file,"stdin")!=0 ){    
               printf("...Skipping back to file %s\n\n",file);
               return -1;
            }
            else       //if it is currently on stdin and user press :p (then skip this file)
               break;
         }
         else if (rv == 9){         //user typed /   (search String Forward)
            //input string then search it
            char* str=dynamicString(fp_tty);
            int a=searchStr(str,fp);
            if(a==1){
               printf("\n...Skipping\n");
               do_more(fp,file,prev,next,10,argc);
               break;
            }
            else if(a==0){
               search=1;      //To clear revMessage in next iteration
               printf("\033[2K\033[1G\033[7mPattern Not Found\033[m"); 
               rv=-1;
               continue;   
            }
            free(str);
         }
         else if (rv == -1){         //invalid character (get input again)
            //printf("\033[2K\033[1G");
            continue; 
        }
        
        if(rv!=-1)
            rv=-3;

         PAGELEN = shell_lines()-1;       //To update PAGELEN if terminal is resized
 
         percStr=1;         
      }
  }
  fclose(fp_tty);
   return -4;
}