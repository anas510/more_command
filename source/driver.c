/* 
                                 MORE(final) version
*****************************************************************************************
===========================CREATED BY ANAS MUHAMMAD(BCSF14M023)==========================
*****************************************************************************************

-----------------------------------------------------------------------------------------
<space>                 Display next k lines of text [k=current screen size]
z                       Display next k lines of text [k=current screen size]
<return>                Display next line of text
q or Q                  Exit from more (quits current file in case of multiple files)
=                       Display current line number
h or ?                  To get help
v                       Start up /usr/bin/vi at current line
:n                      Go to next file
:p                      Go to previous file OR review current file
                        (if ":p" pressed for stdin, remaining file pointed to by stdin will
                        be skipped)
:f                      Display current file name and line number
/<search_string>        Search for string in file (first occurence), after current position
                        and also print above x2 character (x=length of line with string)
-------------------------------------------------------------------------------------------

Extra Tasks performed:
=>move to prev file                    (:p)
=>move to next file                    (:n)
=>Display current Line#                (=)
=>display fileName and current Line#   (:f)
=> z == <space>
=> ? == h
=> Q == q
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <main.h>
#include <global.h>

int main(int argc , char *argv[])
{
   int i=0;
   int rv=-2;
   short isTty=isatty(STDIN_FILENO);
   
   if (!isTty){         //For IO redirection
      PAGELEN = shell_lines()-4;       //3 lines to show fileName
      printf("::::::::::::::::::::::::::::::\n");
      printf("%s","stdin");
      printf("\n::::::::::::::::::::::::::::::\n");
      if(argc>1){
         i=do_more(stdin,"stdin",NULL,argv[1],rv,argc);
      }
      else
         i=do_more(stdin,"stdin",NULL,NULL,rv,argc);
   }

   FILE * fp;
   if(i==-4)      //-4 means do_more returned normally (complete file is read without skipping anything till EOF)
      i=0;
   lines_displayed=0;      //For IO Redirection
   short skip=0;
   while(++i < argc){
      char* next=NULL,*prev=NULL;
      if(i+1<argc)
         next=argv[i+1];
      if(i>1)
         prev=argv[i-1];

      if((i < argc && i!=1) || skip || !isTty){
         printf("\033[7m --More-- (Next file: %s) \033[m", argv[i]);       //to show info about next file if any
         skip=0;
         FILE* fp_tty = fopen("/dev/tty", "r");

         rv=get_input(fp_tty);
         
         short br=0;
         while(rv!=1 && rv !=2){
            int r=process_multiFile_input(argv[i],prev,next, rv,argc);

            if(r==2)
               exit(EXIT_SUCCESS);
            else if(r!=-3){
               i+=r;
               br=1;
               fclose(fp_tty);
               break;
            }

            rv=get_input(fp_tty);

         }
         if(br==1){
            skip=1;
            continue;
         }

         fclose(fp_tty);
      }

      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
      //add fol one line
      total_lines = total_size(fp);

      if(rv==1 || rv ==2){
         printf("\033[2K\033[1G");
      }

      if(argc>2 || !isTty){    //To display file_name
         printf("::::::::::::::::::::::::::::::\n");
         printf("%s",argv[i]);
         printf("\n::::::::::::::::::::::::::::::\n");
      }

      
      PAGELEN = shell_lines()-1;
      
      if(argc>2)
         PAGELEN -= 3;

      int ret=do_more(fp, argv[i],prev,next, rv,argc);
      if(ret <=0 && ret>=-2)
         i+=ret;
      fclose(fp);

      if(ret==-2 || ret==-1 || ret==0)
         skip=1;
      total_lines=0;
      lines_displayed=0;
   }
   return 0;
}

/*
*****************************************************************************************
===========================CREATED BY ANAS MUHAMMAD(BCSF14M023)==========================
*****************************************************************************************
*/