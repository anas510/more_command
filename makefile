all: mymore

#which compiler
CC=gcc

#options for development	(allow debugging and turn off optimization)
CFLAGS= -std=gnu99 -O0 -Wall -g
#options for release
#CFLAGS= -std=gnu99 -O3 -Wall

#where are include files kept
INCLUDES=./includes/

#Include Files
INC_MAIN=./includes/main.h
INC_GLOBAL=./includes/global.h

#object files to be linked inside dynamic library
OBJS= do_more.o multifile.o input.o search.o size.o vim.o input_mode.o help.o

#location of install directory if any
INSTDIR = /usr/local/bin

#libraries to be linked
LIBS = -lmorefunctions

mymore: driver.o libmorefunctions.so
	$(CC) driver.o $(LIBS) -L. -o $@
driver.o: ./source/driver.c $(INC_MAIN) $(INC_GLOBAL)
	$(CC) -c $< -I$(INCLUDES) $(CFLAGS) -o $@
libmorefunctions.so:	$(OBJS)
	$(CC) -shared $(OBJS) -o $@
do_more.o: ./source/do_more.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
input_mode.o: ./source/input_mode.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
search.o: ./source/search.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
vim.o: ./source/vim.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
help.o: ./source/help.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
multifile.o: ./source/multifile.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
size.o: ./source/size.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@
input.o: ./source/input.c $(INC_MAIN)
	$(CC) -c -fPIC $< -I$(INCLUDES) $(CFLAGS) -o $@

clean:
	@-rm -f *.o
	@echo "All .o files deleted successfully."

install: mymore
	@cp mymore $(INSTDIR)
	@cp libmorefunctions.so /usr/lib/x86_64-linux-gnu/
	@chmod a+x $(INSTDIR)/mymore
	@chmod og-w $(INSTDIR)/mymore
	@echo "mymore successfully installed in $(INSTDIR)"

uninstall:
	@rm $(INSTDIR)/mymore
	@rm /usr/lib/x86_64-linux-gnu/libmorefunctions.so
	@echo "mymore uninstalled successfully."