#ifndef MAIN_H
	#define MAIN_H
	
	int do_more(FILE *,char* ,char*,char*,int,int);
	int  get_input(FILE*);
	int process_multiFile_input(char* ,char*,char*,int,int);

	void help();
	void launch_vim(char*);
	long total_size(FILE*);       //returns total_lines count of current file
	int shell_lines();            //to get terminal size (row_count)

	void reset_input_mode ();     //to change input mode back to canonical
	void set_input_mode ();       //to change input mode to non-canonical     

	char* dynamicString(FILE*);  //To get Search String
	int searchStr(char*,FILE*);  //To search String and return 1 if found and 0 if not
#endif